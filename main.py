def simplify_data():
    with open(file, 'r') as txt:
        for line in txt:
            arr = line.split(';')
            data.append({"player": arr[0], "team": arr[1], "time": arr[2]})


if __name__ == "__main__":
    file = "goals.txt"
    data = []
    simplify_data()


def get_team_goals(team):
    count = sum(1 for goal in data if goal["team"] == team)
    print("the team", team, "scored", count, "goals")


def get_player_goals(player):
    count = sum(1 for goal in data if goal["player"] == player)
    print("the player", player, "scored", count, "goals")


def get_players_of_team(team):
    player_list = list({goal["player"] for goal in data if goal["team"] == team})
    print('players of', team, 'that scored a goal:')
    print(*player_list, sep='\n')


def get_goals():
    goals = len(data)
    print('Total Number of goals:', goals)


def get_first_half_goals():
    count = sum(1 for goal in data if int(goal["time"]) <= 45)
    print("there were", count, "goals before halftime")


def get_second_half_goals():
    count = sum(1 for goal in data if 45 < int(goal["time"]) <= 90)
    print("there were", count, "goals after halftime")


def get_overtime_goals():
    count = sum(1 for goal in data if int(goal["time"]) > 90)
    print("there were", count, "goals after overtime")


def main():
    options = """ 
    2018 WORLD CUP:
                
    what do you want to check?
    1) number of goals a team made
    2) number of goals a player made
    3) list of players who made goals in a certain team
    4) number of goals made overall
    5) number of goals at the first half
    6) number of goals at the second half
    7) number of goals after overtime
    8) close
    """
    user_choice = int(input(options))

    while user_choice != 8:
        match user_choice:
            case 1:
                team = input('choose a team')
                get_team_goals(team)
            case 2:
                player = input('choose a player')
                get_player_goals(player)
            case 3:
                team = input('choose a team')
                get_players_of_team(team)
            case 4:
                get_goals()
            case 5:
                get_first_half_goals()
            case 6:
                get_second_half_goals()
            case 7:
                get_overtime_goals()
            case _:
                print('enter valid option')

        user_choice = int(input(options))


main()
